/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.testwx.entity;

import org.hibernate.validator.constraints.Length;

import com.empire.emsite.common.persistence.DataEntity;

/**
 * 类TestWxData.java的实现描述： testwxEntity
 * 
 * @author arron 2017年10月30日 下午1:03:22
 */
public class TestWxData extends DataEntity<TestWxData> {

    private static final long serialVersionUID = 1L;
    private String            openId;               // open_id
    private String            name;                 // 昵称

    public TestWxData() {
        super();
    }

    public TestWxData(String id) {
        super(id);
    }

    @Length(min = 0, max = 255, message = "open_id长度必须介于 0 和 255 之间")
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Length(min = 0, max = 255, message = "昵称长度必须介于 0 和 255 之间")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
