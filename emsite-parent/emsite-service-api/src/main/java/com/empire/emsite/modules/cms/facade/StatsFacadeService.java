/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade;

import java.util.List;
import java.util.Map;

import com.empire.emsite.modules.cms.entity.Category;

/**
 * 类StatsFacadeService.java的实现描述：统计FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:44:28
 */
public interface StatsFacadeService {
    public List<Category> article(Map<String, Object> paramMap, String siteId);
}
