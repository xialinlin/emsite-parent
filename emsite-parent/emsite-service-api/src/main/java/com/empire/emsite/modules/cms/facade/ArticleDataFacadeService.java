/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade;

import com.empire.emsite.modules.cms.entity.ArticleData;

/**
 * 类ArticleDataFacadeService.java的实现描述：站点FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:41:59
 */
public interface ArticleDataFacadeService {
    public ArticleData get(String id);
}
