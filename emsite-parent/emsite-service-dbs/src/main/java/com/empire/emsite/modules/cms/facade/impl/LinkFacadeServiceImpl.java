/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Link;
import com.empire.emsite.modules.cms.facade.LinkFacadeService;
import com.empire.emsite.modules.cms.service.LinkService;

/**
 * 类LinkFacadeServiceImpl.java的实现描述：链接FacadeService接口实现类
 * 
 * @author arron 2017年9月17日 下午9:45:45
 */
@Service
public class LinkFacadeServiceImpl implements LinkFacadeService {
    @Autowired
    private LinkService linkService;

    @Override
    public Link get(String id) {
        return linkService.get(id);
    }

    @Override
    public Page<Link> findPage(Page<Link> page, Link link, boolean isDataScopeFilter) {
        return linkService.findPage(page, link, isDataScopeFilter);
    }

    @Override
    public void delete(Link link, Boolean isRe) {
        linkService.delete(link, isRe);
    }

    @Override
    public void save(Link link) {
        linkService.save(link);
    }

    /**
     * 通过编号获取内容标题
     */
    @Override
    public List<Object[]> findByIds(String ids) {
        return linkService.findByIds(ids);
    }

}
