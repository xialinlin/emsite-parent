/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.service;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.service.CrudService;
import com.empire.emsite.modules.cms.dao.SiteDao;
import com.empire.emsite.modules.cms.entity.Site;

/**
 * 类SiteService.java的实现描述：站点Service
 * 
 * @author arron 2017年10月30日 下午4:11:58
 */
@Service
@Transactional(readOnly = true)
public class SiteService extends CrudService<SiteDao, Site> {

    @Override
    public Page<Site> findPage(Page<Site> page, Site site) {
        //		DetachedCriteria dc = siteDao.createDetachedCriteria();
        //		if (StringUtils.isNotEmpty(site.getName())){
        //			dc.add(Restrictions.like("name", "%"+site.getName()+"%"));
        //		}
        //		dc.add(Restrictions.eq(Site.FIELD_DEL_FLAG, site.getDelFlag()));
        //		//dc.addOrder(Order.asc("id"));
        //		return siteDao.find(page, dc);

        site.getSqlMap().put("site", dataScopeFilter(site.getCurrentUser(), "o", "u"));

        return super.findPage(page, site);
    }

    @Override
    @Transactional(readOnly = false)
    public void save(Site site) {
        if (site.getCopyright() != null) {
            site.setCopyright(StringEscapeUtils.unescapeHtml4(site.getCopyright()));
        }
        super.save(site);
    }

    @Transactional(readOnly = false)
    public void delete(Site site, Boolean isRe) {
        //siteDao.updateDelFlag(id, isRe!=null&&isRe?Site.DEL_FLAG_NORMAL:Site.DEL_FLAG_DELETE);
        site.setDelFlag(isRe != null && isRe ? Site.DEL_FLAG_NORMAL : Site.DEL_FLAG_DELETE);
        super.delete(site);
        //siteDao.delete(id);
    }

}
