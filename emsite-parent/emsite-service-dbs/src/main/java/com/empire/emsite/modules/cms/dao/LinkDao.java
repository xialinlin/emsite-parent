/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.dao;

import java.util.List;

import com.empire.emsite.common.persistence.CrudDao;
import com.empire.emsite.common.persistence.annotation.MyBatisDao;
import com.empire.emsite.modules.cms.entity.Link;

/**
 * 类LinkDao.java的实现描述：链接DAO接口
 * 
 * @author arron 2017年10月30日 下午4:08:06
 */
@MyBatisDao
public interface LinkDao extends CrudDao<Link> {

    public List<Link> findByIdIn(String[] ids);

    //	{
    //		return find("front Like where id in (:p1)", new Parameter(new Object[]{ids}));
    //	}

    public int updateExpiredWeight(Link link);
    //	{
    //		return update("update Link set weight=0 where weight > 0 and weightDate < current_timestamp()");
    //	}
    //	public List<Link> fjindListByEntity();
}
