/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.empire.emsite.common.service.BaseService;
import com.empire.emsite.common.utils.DateUtils;
import com.empire.emsite.modules.cms.dao.ArticleDao;
import com.empire.emsite.modules.cms.entity.Category;
import com.empire.emsite.modules.cms.entity.Site;
import com.empire.emsite.modules.sys.entity.Office;

/**
 * 类StatsService.java的实现描述：统计Service
 * 
 * @author arron 2017年10月30日 下午4:12:08
 */
@Service
@Transactional(readOnly = true)
public class StatsService extends BaseService {

    @Autowired
    private ArticleDao articleDao;

    public List<Category> article(Map<String, Object> paramMap, String siteId) {
        Category category = new Category();

        Site site = new Site();
        site.setId(siteId);
        category.setSite(site);

        Date beginDate = DateUtils.parseDate(paramMap.get("beginDate"));
        if (beginDate == null) {
            beginDate = DateUtils.setDays(new Date(), 1);
            paramMap.put("beginDate", DateUtils.formatDate(beginDate, "yyyy-MM-dd"));
        }
        category.setBeginDate(beginDate);
        Date endDate = DateUtils.parseDate(paramMap.get("endDate"));
        if (endDate == null) {
            endDate = DateUtils.addDays(DateUtils.addMonths(beginDate, 1), -1);
            paramMap.put("endDate", DateUtils.formatDate(endDate, "yyyy-MM-dd"));
        }
        category.setEndDate(endDate);

        String categoryId = (String) paramMap.get("categoryId");
        if (categoryId != null && !("".equals(categoryId))) {
            category.setId(categoryId);
            category.setParentIds(categoryId);
        }

        String officeId = (String) (paramMap.get("officeId"));
        Office office = new Office();
        if (officeId != null && !("".equals(officeId))) {
            office.setId(officeId);
            category.setOffice(office);
        } else {
            category.setOffice(office);
        }

        List<Category> list = articleDao.findStats(category);
        return list;
    }

}
