/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.service.CrudService;
import com.empire.emsite.modules.cms.dao.CommentDao;
import com.empire.emsite.modules.cms.entity.Comment;

/**
 * 类CommentService.java的实现描述：评论Service
 * 
 * @author arron 2017年10月30日 下午4:10:19
 */
@Service
@Transactional(readOnly = true)
public class CommentService extends CrudService<CommentDao, Comment> {

    @Override
    public Page<Comment> findPage(Page<Comment> page, Comment comment) {
        //		DetachedCriteria dc = commentDao.createDetachedCriteria();
        //		if (StringUtils.isNotBlank(comment.getContentId())){
        //			dc.add(Restrictions.eq("contentId", comment.getContentId()));
        //		}
        //		if (StringUtils.isNotEmpty(comment.getTitle())){
        //			dc.add(Restrictions.like("title", "%"+comment.getTitle()+"%"));
        //		}
        //		dc.add(Restrictions.eq(Comment.FIELD_DEL_FLAG, comment.getDelFlag()));
        //		dc.addOrder(Order.desc("id"));
        //		return commentDao.find(page, dc);
        comment.getSqlMap().put("dsf", dataScopeFilter(comment.getCurrentUser(), "o", "u"));

        return super.findPage(page, comment);
    }

    @Transactional(readOnly = false)
    public void delete(Comment entity, Boolean isRe) {
        super.delete(entity);
    }
}
