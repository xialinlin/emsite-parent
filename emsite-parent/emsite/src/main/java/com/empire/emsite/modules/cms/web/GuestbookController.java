/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.entity.Guestbook;
import com.empire.emsite.modules.cms.facade.GuestbookFacadeService;
import com.empire.emsite.modules.sys.utils.DictUtils;
import com.empire.emsite.modules.sys.utils.UserUtils;

/**
 * 类GuestbookController.java的实现描述：留言Controller
 * 
 * @author arron 2017年10月30日 下午7:11:40
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/guestbook")
public class GuestbookController extends BaseController {

    @Autowired
    private GuestbookFacadeService guestbookFacadeService;

    @ModelAttribute
    public Guestbook get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return guestbookFacadeService.get(id);
        } else {
            return new Guestbook();
        }
    }

    @RequiresPermissions("cms:guestbook:view")
    @RequestMapping(value = { "list", "" })
    public String list(Guestbook guestbook, HttpServletRequest request, HttpServletResponse response, Model model) {
        guestbook.setCurrentUser(UserUtils.getUser());
        Page<Guestbook> page = guestbookFacadeService.findPage(new Page<Guestbook>(request, response), guestbook);
        model.addAttribute("page", page);
        return "modules/cms/guestbookList";
    }

    @RequiresPermissions("cms:guestbook:view")
    @RequestMapping(value = "form")
    public String form(Guestbook guestbook, Model model) {
        model.addAttribute("guestbook", guestbook);
        return "modules/cms/guestbookForm";
    }

    @RequiresPermissions("cms:guestbook:edit")
    @RequestMapping(value = "save")
    public String save(Guestbook guestbook, Model model, RedirectAttributes redirectAttributes) {
        if (!beanValidator(model, guestbook)) {
            return form(guestbook, model);
        }
        if (guestbook.getReUser() == null) {
            guestbook.setReUser(UserUtils.getUser());
            guestbook.setReDate(new Date());
        }
        guestbook.setCurrentUser(UserUtils.getUser());
        guestbookFacadeService.save(guestbook);
        addMessage(redirectAttributes, DictUtils.getDictLabel(guestbook.getDelFlag(), "cms_del_flag", "保存") + "留言'"
                + guestbook.getName() + "'成功");
        return "redirect:" + adminPath + "/cms/guestbook/?repage&status=2";
    }

    @RequiresPermissions("cms:guestbook:edit")
    @RequestMapping(value = "delete")
    public String delete(Guestbook guestbook, @RequestParam(required = false) Boolean isRe,
                         RedirectAttributes redirectAttributes) {
        guestbookFacadeService.delete(guestbook, isRe);
        addMessage(redirectAttributes, (isRe != null && isRe ? "恢复审核" : "删除") + "留言成功");
        return "redirect:" + adminPath + "/cms/guestbook/?repage&status=2";
    }

}
